### Yazan:      Mert Ünal
#   Numara:     15401687
#   Ödev Nu:    5
#   Versiyon:   1.0.0
#   Commit Nu:  1


#######################################################################################################################
#
#   DIKKAT: Duzgun calismasi icin 4. odevde benim gonderdigim "protokol.py" isimli serverla birlikte calistirilmalidir !!
#
#######################################################################################################################

import sys
import socket
import threading
from PyQt5.QtCore import*
from PyQt5.QtGui import*
from PyQt5.QtWidgets import*
import queue
import time
import functools

class ReadThread (threading.Thread):
    def __init__(self, name, csoc, writeQueue, screenQueue, app):
        threading.Thread.__init__(self)
        self.name = name
        self.csoc = csoc
        self.nickname = ""
        self.writeQueue = writeQueue
        self.screenQueue = screenQueue
        self.app = app

    def incoming_parser (self, data):
        if len(data) == 0:
            return
        rest = data[3:]

        if data[0:2] == "ER":
            anlamli_data = "Bir hata olustu!"
        elif data[0:2] == "EL":
            anlamli_data = "Giris yapiniz."
        elif data[0:2] == "BY":
            anlamli_data = rest+" olarak cikis yapildi."
        elif data[0:2] == "TO":
            anlamli_data = "Baglanti basarili."
        elif data[0:2] == "UR":
            anlamli_data = rest+" nickli bir kullanici yoktur."
        elif data[0:2] == "UN":
            anlamli_data = rest+" kullanicisi icin yanlis sifre girdiniz."
        elif data[0:2] == "UO":
            anlamli_data = "Giris yapildi. Hosgeldin "+rest
        elif data[0:2] == "RN":
            anlamli_data = rest+" nickli bir kullanici zaten mevcut! Lutfen baska nick seciniz."
        elif data[0:2] == "RO":
            anlamli_data = "Uyelik islemi basarili! Hosgeldin "+rest
        elif data[0:2] == "CO":
            anlamli_data = "Sifre degistirme islemi basarili"
        elif data[0:2] == "CN":
            anlamli_data = "Sifre degistirme basarisiz!"
        elif data[0:2] == "GO":
            anlamli_data = "Mesaj herkese gonderildi"
        elif data[0:2] == "PO":
            anlamli_data = "Ozel mesaj gonderildi."
        elif data[0:2] == "PN":
            anlamli_data = rest+" nickli bir kullanici yok."
        elif data[0:2] == "SM":
            anlamli_data = "Sistem mesajı: "+rest
        elif data[0:2] == "GM":
            for i in range(0, len(rest)-1):
                if rest[i] == " ":
                    break
            gonderen = rest[0:i]
            text = rest[i+4:]
            anlamli_data = "Genel mesaj: "+gonderen+": "+text
        elif data[0:2] == "PM":
            for i in range(0, len(rest)-1):
                if rest[i] == " ":
                    break
            gonderen = rest[0:i]
            text = rest[i+4:]
            anlamli_data = "Ozel mesaj: "+gonderen+": "+text
        elif data[0:2] == "LA":
            splitted = rest.split(":")
            nicks = ""
            for i in splitted:
                nicks = nicks + i + ", "
            nicks = nicks[:-2]
            anlamli_data = "Online kisiler: "+nicks

        anlamli_data = "-Server- "+anlamli_data
        return anlamli_data

    def run(self):
        self.screenQueue.put("DIKKAT: Duzgun calismasi icin 4. odevde benim gonderdigim 'protokol.py' isimli serverla birlikte calistirilmalidir !!")
        while True:
            incoming_data = self.csoc.recv(1024)
            meanful_data = self.incoming_parser(incoming_data.decode().strip())

            self.screenQueue.put(meanful_data)

class WriteThread (threading.Thread):
    def __init__(self, name, csoc, writeQueue):
        threading.Thread.__init__(self)
        self.name = name
        self.csoc = csoc
        self.writeQueue = writeQueue

    def run(self):
        while True:
            queueMessage = self.writeQueue.get()
            self.csoc.send(queueMessage.encode())
            if queueMessage[0:2] == "BY": # eğer gönderilen response BY ile başlıyorsa while döngüsünü kırıyoruz ki çıkış yapalım.
                break

class ClientDialog(QDialog):
    def __init__(self, writeQueue, screenQueue):
        self.writeQueue = writeQueue
        self.screenQueue = screenQueue

        self.qt_app = QApplication(sys.argv)

        QDialog.__init__(self, None)

        self.setWindowTitle('IRC Client')
        self.setMinimumSize(500, 200)
        self.vbox = QVBoxLayout()

        self.sender = QLineEdit(self) #bu önemli bi şeye benziyor.

        self.channel = QTextBrowser() #bunu updateText'te kullanmış. kanal bölgesi diyor. mesajların render edildiği yer.

        self.send_button = QPushButton('&Send') #buton

        self.send_button.clicked.connect(self.outgoing_parser) #onClick işlemi

        self.vbox.addWidget(self.channel)       ##
        self.vbox.addWidget(self.sender)        ## burada yarattığı componentleri layouta basmış. önemi yok
        self.vbox.addWidget(self.send_button)   ##
        self.setLayout(self.vbox)

        self.timer = QTimer()
        func = functools.partial(self.updateText)
        self.timer.timeout.connect(func)     ## zamanlayıcı çağırmış. 10 ms'de bir updateText'i çağırmış.
        self.timer.start(10)

    def updateText(self):
        if not self.screenQueue.empty():
            data = self.screenQueue.get()
            t = time.localtime()
            pt = "%02d:%02d" % (t.tm_hour, t.tm_min)
            self.channel.append(pt + " " + data)
        else:
            return

    def outgoing_parser(self):
        data = self.sender.text()
        self.screenQueue.put("Local: "+data)
        if len(data) == 0:
            return
        if data[0] == "/":
            for i in range (0, len(data)-1):
                if data[i] == " ":
                    break
            command = data[1:i]
            if command == "list":
                self.writeQueue.put("LQ")
            elif command == "quit":
                self.writeQueue.put("QU")
            elif command == "msg":
                for j in range(5, len(data)-1):
                    if data[j] == " ":
                        break
                nick = data[5:j]
                text = data[j+1:]
                self.writeQueue.put("PM "+nick+":"+text)
            elif command == "register":
                for k in range(10, len(data)-1):
                    if data[k] == " ":
                        break
                nick = data[10:k]
                passw = data[k+1:]
                self.writeQueue.put("RG "+nick+":"+passw)
            elif command == "nick":
                for k in range(6, len(data)-1):
                    if data[k] == " ":
                        break
                nick = data[6:k]
                passw = data[k+1:]
                self.writeQueue.put("US "+nick+":"+passw)
            else:
                self.screenQueue.put("Local: Command Error.")
                self.screenQueue.put(command)
        else:
            self.writeQueue.put("GM "+data)
        self.sender.clear()
        #

    def run(self):
        self.show()
        self.qt_app.exec_()

def main():
    #server baglantisi.
    s = socket.socket()
    host = "127.0.0.1"
    port = 2266
    s.connect((host, port))

    screenQueue = queue.Queue()
    writeQueue = queue.Queue()

    app = ClientDialog(writeQueue, screenQueue)

    rt = ReadThread("ReadThread", s, writeQueue, screenQueue, app)
    rt.start()

    wt = WriteThread("WriteThread", s, writeQueue)
    wt.start()

    app.run()
    rt.join()
    wt.join()

    s.close()

if __name__ == "__main__":
    main()















