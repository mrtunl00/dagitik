### Yazan:      Mert Ünal
#   Numara:     15401687
#   Ödev Nu:    4
#   Versiyon:   1.0.1
#   Commit Nu:  2

import threading
import socket
import queue
from datetime import datetime

class LogThread(threading.Thread): #bu bizim logları tutacak thread.
    def __init__(self, logQueue):
        threading.Thread.__init__(self)
        self.lqueue = logQueue

    def run(self):
        f = open("serverlogs.txt", "w")
        f.close() #dosyayı bir aç-kapa yapıyoruz ki server önceden çalıştığında kalan loglar temizlensin.
        while True:
            f = open("serverlogs.txt", "a") #thread'in sonsuz döngüsünde bu kez dosyayı append modunda açıyoruz.
            queueMessage = self.lqueue.get() # mainde yarattığımız log kuyruğundan item çekiyoruz.
            nowString = datetime.now().strftime("%d/%m/%Y %H:%M:%S") # o anki saati ve tarihi çekiyoruz.
            f.write(nowString + " " + queueMessage + "\n") # dosyaya logları yazıyoruz.
            f.close()


class WriteThread(threading.Thread):# kuyruktaki mesajlari ait olduğu prosese yazmak icin kullanilacak
    def __init__(self, name, csoc, address, threadQueue, logQueue):
        threading.Thread.__init__(self)
        self.name = name
        self.csoc = csoc
        self.address = address
        self.lqueue = logQueue
        self.tqueue = threadQueue

    def run(self):
        self.lqueue.put("Starting " + self.name) # lqueue'nun bütün put operasyonları, log kuyruğuna yeni veri pushlamak. bir daha yazmayacağım bunu.
        while True:
            queueMessage = self.tqueue.get() #ait olduğu prosesin threadqueue'sundan veriyi çektik.
            self.csoc.send((queueMessage + '\n').encode()) # veriyi gönderiyoruz.
            if queueMessage[0:2] == "BY": # eğer gönderilen response BY ile başlıyorsa while döngüsünü kırıyoruz ki çıkışı log queue'ya yazalım.
                break

        self.lqueue.put("Exiting " + self.name)


class ReadThread(threading.Thread):
    def __init__(self, name, csoc, address, threadQueue, logQueue, fihrist):
        threading.Thread.__init__(self)
        self.name = name
        self.nickname = None
        self.csoc = csoc
        self.address = address
        self.tQueue = threadQueue
        self.lqueue = logQueue
        self.kullaniciFihristi = fihrist

    def parser(self, data):
        # bu methodda şöyle bir durum var: en başa RG komutunu koyuyoruz diğerlerini else yapısıyla kuruyoruz
        # aksi halde en üstte iskelet koddaki gibi elif not self.nickname'li ifade olduğunda RG komutuna da EL cevabı döndürüyordu.
        # böyle basit bir çözüm düşündüm buna o yüzden.

        if data[0: 2] == "RG":
            for i in range(0, len(data) - 1):
                if data[i] == ":":
                    break # gelen komut içinde ":" ifadesini arıyoruz. bulduğumuz anda kesiyoruz.

            if i != len(data) - 1:
                tempnickname = data[3:i]    #i'ye kadar olan kısım nick olsun, sonrası da şifre olur böylece diyoruz.
                temppassword = data[i + 1:] # bu yapıyı US'ta ve şifre değiştirmede de kullandım.

                userExist = False
                for user in self.kullaniciFihristi:
                    if tempnickname == user:
                        return "RN " + user + "\n" #kullanıcı fihristine bakıyoruz. eğer soketten gelen kullanıcı adı verisi zaten bir kullanıcıya aitse
                        userExist = True           # ilgili cevabı döndürüyoruz.
                        break

                if not userExist:   # eğer verilen nickname fihristte yoksa userExist false kalıyor ve buraya giriyor. bunu yapmak zorundayız aksi halde yukarıdaki for bitince her
                                    # halükarda buraya girerdi ve var olan kullanıcının verilerini değiştirirdi.
                    self.kullaniciFihristi[tempnickname] = [temppassword, None, None] #yeni kullanıcı yaratıyoruz. ikimnci ve üçüncü array elemanı şu an boş. user sign in olduğunda dolacaklar.
                    self.lqueue.put("New User: "+tempnickname) # yeni kullanıcı logu
                    return "RO " + tempnickname + "\n" # ilgili cevap

        elif not self.nickname and not data[0:2] == "US":
            return "EL\n"

        if data[0:2] == "US":
            for i in range(0, len(data) - 1): #yukarıda açıkladığım yapının aynısı
                if data[i] == ":":
                    break

            if i != len(data) - 1:
                tempnickname = data[3:i] #yukarıda açıkladığım yapımım aynısı
                temppassword = data[i + 1:]

                for user in self.kullaniciFihristi:                                 # fihristimizdeki kullanıcılara bakıyoruz. eğer girilen kullanıcı adı gerçekten varsa
                    if tempnickname == user:                                        # bu sefer şifre doğruluğuna bakıyoruz. o da doğruysa kullanıcının threadQueuesunu ayarlayıp
                        if temppassword == self.kullaniciFihristi[user][0]:         # loginState'ini True'ya çekiyoruz. Artık kullanıcı online.
                            self.kullaniciFihristi[user][1] = self.tQueue
                            self.kullaniciFihristi[user][2] = True
                            self.nickname = tempnickname                            # tabi algoritma gereği self.nickname variable'ını güncellemeyi unutmuyoruz.
                            response = "UO " + user + "\n"
                            self.lqueue.put("User signed in: " + tempnickname)
                            break
                        else:
                            response = "UN " + user + "\n"  #eğer şifre yanlışsa UN veriyoruz.

                    else:
                        response = "UR " + tempnickname + "\n" #eğer böyle bi kullanıcı adında hesap yoksa UR veriyoruz.
                if len(self.kullaniciFihristi.keys()) == 0:     #eğer fihrist boşsa UR veriyoruz. bunu yapmamın sebebi, eğer fihrist boşsa yukarıda
                    response = "UR " + tempnickname + "\n"      #for döngüsüne girmiyordu. o yüzden.
            else:
                response = "ER\n" # diğer durumlar için ER ayarladık. mesela adam kullanıcı adını yazıp şifreyi yazmazsa falan.



        elif data[0:2] == "TI": # bu gayet açık.
            response = "TO\n"

        elif data[0:2] == "QU":                                     #kullanıcı çıkış yapmak istiyorsa bi defa onun threadQueue'sunu yok etmemiz gerekir. yer kaplamasın.
            self.lqueue.put("Neutralising: " + self.name)           #tabi çıkış yaptığı için loginState'ini de false'a çekiyoruz.
            self.kullaniciFihristi[self.nickname][1] = None
            self.kullaniciFihristi[self.nickname][2] = False
            self.lqueue.put("User Logged Out: " + self.nickname)    #log dosyasına çıkış işlemini yazıyoruz.
            response = "BY " + self.nickname + "\n"                 #ilgili response
            self.nickname = None                                    #tabi ki self.nickname variable'ını güncellemeyi unutmuyoruz ki çıkış yaptıktan sonra komutları giremesin
                                                                    #yukarıda da dediğim gibi, algoritma gereği..

        elif data[0:2] == "LQ":
            ActiveUsers = []
            for user in self.kullaniciFihristi:                 #bütün fihristi geziyoruz. loginState'i true olan yani online olan herkesi ActiveUsers array'ine atıyoruz.
                if self.kullaniciFihristi[user][2] == True:
                    ActiveUsers.append(user)

            activeUsersString = ""
            for x in ActiveUsers:               #array'in içindeki elemanları yan yana diziyoruz.
                activeUsersString += x + ":"

            response = "LA " + activeUsersString + "\n" #ilgili cevap

        elif data[0:2] == "CP":
            for i in range(0, len(data) - 1):
                if data[i] == ":":         # 2 argümanlı her komutta bunu yaptığım anlaşılmıştır. burada da eski şifreyle yeni şifreyi ayırmak için ":" arıyoruz gelen komutta.
                    break

            if i != len(data) - 1:
                tempoldpassword = data[3:i]     #yukarıdaki yapının aynısı. bu kez eski şifre ve yeni şifreyi belirledik.
                tempnewpassword = data[i + 1:]
                if self.kullaniciFihristi[self.nickname][0] != tempoldpassword: #eğer eski şifreyi kullanıcı yanlış yazdıysa işlemi yapmıyoruz.
                    response = "CN\n"
                else:
                    self.kullaniciFihristi[self.nickname][0] = tempnewpassword #eğer doğru yazdıysa ilgili kullanıcının şifresini güncelliyoruz.
                    response = "CO\n"
            else:
                response = "ER\n"

        elif data[0:2] == "GM":
            for user in self.kullaniciFihristi:             # eğer genel mesaj komutu geldiyse bütün fihristte aktif olan kullanıcıları bulup bunların threadQueue'larına
                if self.kullaniciFihristi[user][2] == True: # mesajı, gönderenin nickiyle beraber yazıyoruz.
                    self.kullaniciFihristi[user][1].put("General>> " + self.nickname + " >> " + data[3:])
            response = "GO\n" #mesajı gönderene cevap.

        elif data[0:2] == "PM":
            to_nickname = ""
            for x in data[3:]:  # 2 argümanı ayırma olayı tekrar. bu kez hedef kullanıcı adı ve mesajı ayırıyoruz.
                if x != ":":
                    to_nickname += x #ama bu kez biraz farklı. karakter karakter gezerken bu kez : görene kadar hedef nickname'i belirliyorz.
                else:
                    break

            if not to_nickname in self.kullaniciFihristi.keys() or not self.kullaniciFihristi[to_nickname][2]: # eğer hedef nickname fihristte yoksa veya hedef online değilse
                response = "PN " + to_nickname + "\n"                                                          # ilgili cevabı gönderene veriyoruz.
            else:
                personalMessage = "Personal>>" + self.nickname + ">>" + data[4 + len(to_nickname):]     #aksi durumda bu formatta mesajı yaratıp
                self.kullaniciFihristi[to_nickname][1].put(personalMessage)                             #hedefin threadQueue'suna yazıyoruz.
                response = "PO\n"

        else:
            response = "ER\n"   #bütün yanlış komut kullanımlarında ER döndürüyoruz.

        return response #belirlediğimiz response'u bu methodun dönüş değeri yapıyoruz. sonra bu response run methodu içinde gönderilecek.

    def run(self):
        self.lqueue.put("Starting " + self.name)
        while True:
            incoming_data = self.csoc.recv(1024)
            queue_message = self.parser(incoming_data.decode().strip()) # ilgili istemciden yeni komut geldiğinde hemen bunu decode edip stripleyip parser methoduna veriyoruz.
                                                                        # geri dönen o "response"u bir variable'a attık.
            try:
                self.kullaniciFihristi[self.nickname][1].put(queue_message) # şimdi burada ilginç bir olay var. dönen response'u ilk başta ilgili istemcinin threadQueue'suna
            except:                                                         # yazmaya çalışıyoruz. fakat bu adam henüz sign in olmadıysa threadQueue'su yok doğal olarak.
                self.csoc.send(queue_message.encode())                      # bu da EL, RN, RO, UN, UR cevaplarında mümkün. dolayısıyla eğer tqueue henüz yoksa send komutuyla
                                                                            # işimizi hallediyoruz.

def main():
    s = socket.socket()

    host = "0.0.0.0"
    port = 2266
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # bunu port'u tekrar kullanabilmek için yazdım. yoksa OS boşaltana kadar bayağı bekliyorsunuz serverı başlatırken.

    s.bind((host, port))
    s.listen()

    kullaniciFihristi = {} # 1 tane fihristimiz olacak. 1 tane log queue var. 1 tane log threadi var. dolayısıyla bunları 1 kere tanımlıyoruz.
    logQueue = queue.Queue()

    loglamaThreadi = LogThread(logQueue)
    loglamaThreadi.start()

    counter = 0

    while True:
        c, addr = s.accept()
                                # fakat tqueue, write thread ve read thread her kullanıcı için unique olmalı. bu yüzden onları burada yaratıp başlatıyoruz.
        yeniThreadQueue = queue.Queue()
        yeniWriteThread = WriteThread("Write Thread-%s" % {counter}, c, addr, yeniThreadQueue, logQueue)
        yeniReadThread = ReadThread("Read Thread-%s" % {counter}, c, addr, yeniThreadQueue, logQueue, kullaniciFihristi)

        yeniWriteThread.start()
        yeniReadThread.start()

        counter += 1


if __name__ == "__main__":
    main()
